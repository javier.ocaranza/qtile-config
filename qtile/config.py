
import os

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

# Custom imports
from custom_widgets.battery import BatteryWidget

mod = "mod4"
terminal = "kitty"
browser = "firefox"

colors = ["#1F1D36",
          "#3F3351",
          "#864879",
          "#E9A6A6"]

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod, "shift"], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "f", lazy.spawn(browser), desc="Launch browser"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Key([mod], "r", lazy.spawncmd(),
    #    desc="Spawn a command using a prompt widget"),
    # Custom shortcuts
    # Rofi
    Key([mod], "space", lazy.spawn("rofi -show run"), desc="Launch Rofi"),

    # Brightness
    Key([], "XF86MonBrightnessDown", lazy.spawn("light -U 5"), desc="Decrease brightness"),
    Key([], "XF86MonBrightnessUp", lazy.spawn("light -A 5"), desc="Increase brightness"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl -- set-sink-volume 0 -5%"), desc="Decrease volume"),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl -- set-sink-volume 0 +5%"), desc="Increase volume"),
    Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute 0 toggle"), desc="Mute volume"),

]

_groups = {1: Group("", matches=[Match(wm_class=["firefox"])]),
           2: Group("", matches=[Match(wm_class=["kitty"])]),
           3: Group("", matches=[Match(wm_class=["jetbrains-pycharm-ce"]),
                                  Match(wm_class=["code-oss"])]),
           4: Group(""),
           5: Group(""),
           6: Group("")}

groups = [_groups[k] for k in _groups]

for num, group in _groups.items():
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], str(num), lazy.group[group.name].toscreen(),
            desc="Switch to group {}".format(group.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], str(num), lazy.window.togroup(group.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(group.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

default_layout_config = {"border_focus_stack": ['#d75f5f', '#8f3d3d'],
                         "border_width": 2,
                         "margin": 5}

layouts = [
    layout.Columns(**default_layout_config),
    layout.Max(**default_layout_config),
    # layout.Stack(num_stacks=2),
    layout.Bsp(**default_layout_config),
    # layout.Matrix(),
    layout.MonadTall(**default_layout_config),
    layout.MonadWide(**default_layout_config),
    # layout.RatioTile(),
    layout.Tile(**default_layout_config),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

### Widgets ###

widget_defaults = dict(
    font='Roboto',
    fontsize=14,
    padding=3,
)

extension_defaults = widget_defaults.copy()

battery_widget = BatteryWidget(
    format='{char} {percent:1.0%}',
    #low_background=colours[1],
    show_short_text=False,
    low_percentage=0.15,
    background=colors[2],
    # foreground=colors[3],
    notify_below=15,
    update_interval=2,
    font="Font Awesome 5 Free",
)

def parse_windows_name(text):
    if len(text) >= 25:
        new_text = text[:25] + "..."
        return new_text
    return text


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(
                    font="Font Awesome 5 Free Solid",
                    fontsize=16,
                    active=colors[3],
                    inactive=colors[1],
                    highlight_method="line",
                    highlight_color=[colors[2]],
                    this_current_screen_border=colors[2]
                ),
                widget.WindowName(
                    parse_text=parse_windows_name
                ),
                widget.CPU(),
                widget.PulseVolume(),
                widget.Systray(),
                widget.BatteryIcon(),
                battery_widget,
                widget.Wlan(
                    interface="wlp2s0",
                    format="{percent:1.0%}"
                ),
                widget.Clock(format='%d-%m %a %I:%M %p'),
                widget.QuickExit(),
            ],
            24,
            background=[colors[0]],
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"


# Functions

# Autostart function
@hook.subscribe.startup
def autostart():
    os.system("feh --bg-scale ~/Imágenes/Wallpapers/Wall1.jpg")
    os.system("picom --experimental-backends -b")
    return
