def parse_windows_name(text):
    if len(text) >= 25:
        text = text[:25] + "..."
        return text

if __name__ == "__main__":
    string = str([i for i in "123456789012345678901234567890"])
    text = parse_windows_name(string)
    print(text)